import yaml

Configuration = ''

with open('./config.yml') as config_file:
    Configuration = yaml.load(config_file, Loader=yaml.FullLoader)
