import numpy
from sklearn.cluster import KMeans
from config import Configuration


class Genetic():
    def __init__(self, dataset):
        super().__init__()
        self.dataset = dataset
        self.MAX_K = len(dataset)
        self.CHROMOSOME_LENGTH = len(numpy.binary_repr(self.MAX_K))
        self.KMEANS_MAX_ITERATION = Configuration['kmeans']['max_iteration']
        self.MUTATION_RATE = Configuration['genetic']['mutation_percentage'] / 100
        self.ELITISM_RATE = Configuration['genetic']['elitism_percentage'] / 100
        self.cached_kmeans = {}

    def population_sse(self, population):
        fitness = []
        for individual in population:
            if str(individual) in self.cached_kmeans.keys():
                fitness.append(self.cached_kmeans[str(individual)])
            else:
                kmeans = KMeans(
                    n_clusters=individual,
                    max_iter=self.KMEANS_MAX_ITERATION,
                    random_state=0,
                ).fit(self.dataset)
                self.cached_kmeans[str(individual)] = kmeans.inertia_
                fitness.append(kmeans.inertia_)
        return fitness

    def select_parents(self, population_fitness):
        fitness = list(map(lambda pf: pf[1], population_fitness))
        fitness_sum = sum(fitness)
        normalized_fitness = [f/fitness_sum for f in fitness]

        choices = numpy.random.choice(
            range(len(population_fitness)),
            10,
            p=normalized_fitness,
        )
        candidate_idx = numpy.random.choice(choices)
        candidate = population_fitness[candidate_idx]

        return (candidate_idx, candidate)

    def crossover(self, population, fitness, offspring_size):
        offsprings = []

        population_count = len(population)
        dtype = [('k', numpy.uint64), ('fitness', numpy.float64)]
        population_fitness = numpy.array(
            list(zip(population, fitness)),
            dtype=dtype,
        )
        sorted_population_fitness = numpy.flip(
            numpy.sort(population_fitness, order='fitness'),
        )
        elites, remaining_population = self.select_elites(
            sorted_population_fitness,
            percentage=self.ELITISM_RATE,
        )
        offsprings.extend(map(lambda e: self.encode(e[0]), elites))

        while len(offsprings) < len(population):
            father_index, father = self.select_parents(remaining_population)
            mother_index, mother = self.select_parents(
                numpy.delete(remaining_population, father_index, 0),
            )
            offspring = self.mate(father[0], mother[0])
            if not self.is_valid_offspring(offspring):
                continue
            offsprings.append(offspring)

        self.mutate_offsprings(
            offsprings,
            percentage=self.MUTATION_RATE,
        )

        return self.decode_offsprings(offsprings)

    def select_elites(self, population_fitness, percentage):
        population_count = len(population_fitness)
        return (
            population_fitness[:int(percentage*population_count)],
            population_fitness[int(percentage*population_count):],
        )

    def mate(self, father, mother):
        crossover_point = numpy.uint64(self.MAX_K//2)
        offspring = ""
        offspring += self.encode(father)[:crossover_point]
        offspring += self.encode(mother)[crossover_point:]
        return offspring

    def encode(self, chromosome: int) -> str:
        return numpy.binary_repr(chromosome, width=self.CHROMOSOME_LENGTH)

    def mutate_offsprings(self, offsprings, percentage):
        for i in range(int(percentage * len(offsprings))):
            mutated = self.mutate_chromosome(offsprings[i])
            while not self.is_valid_mutation(mutated):
                mutated = self.mutate_chromosome(offsprings[i])
            offsprings[i] = mutated

    def mutate_chromosome(self, chromosome):
        new_choromosome = ''
        for b in chromosome:
            must_mutate = numpy.random.default_rng().random() > 0.5
            if must_mutate:
                new_choromosome += '0' if b == '1' else '1'
            else:
                new_choromosome += b
        return new_choromosome

    def decode_offsprings(self, offsprings):
        return [int(offspring, 2) for offspring in offsprings]

    def is_valid_mutation(self, mutated_chromosome):
        return 0 < int(mutated_chromosome, 2) < self.MAX_K + 1

    def is_valid_offspring(self, offspring):
        return 0 < int(offspring, 2) < self.MAX_K + 1
