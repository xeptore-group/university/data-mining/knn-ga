from json import dumps
import numpy
from config import Configuration
import genetic


MAX_DATASET = Configuration['general']['max_dataset_size']
dataset = []
with open('./dataset/3D_spatial_network.txt') as rf:
    lines_read = 0
    while (line:= rf.readline()) and lines_read < MAX_DATASET:
        parts = line.split(',')
        data = (float(parts[1]), float(parts[2]))
        if data in dataset:
            continue
        dataset.append(data)
        lines_read += 1

POPULATION_SIZE = Configuration['genetic']['population_size']
MAX_K = len(dataset)

ga = genetic.Genetic(dataset)

if Configuration['genetic']['initial_population_include_max_k']:
    random_population_high_bound = MAX_K + 1
else:
    random_population_high_bound = MAX_K
population = numpy.random.default_rng().integers(
    low=1,
    high=random_population_high_bound,
    size=POPULATION_SIZE,
    dtype=numpy.uint,
)
population_sse = ga.population_sse(population)

generation = 0

while True:
    print(f'> Generation: {generation}')
    print(f'> Min SSE: {min(population_sse)*1e10:.5f}')
    if any(map(lambda x: x == 0.0, population_sse)):
        print('> Answer Found!')
        print(f'> Answer Individual: {population[0]} - ({numpy.binary_repr(population[0])})')
        formatted_population = dumps(list(zip(population, population_sse)), indent=2)
        print(f'> Heroes Population: {formatted_population}')
        break

    generation += 1

    population_fitness = [1/sse for sse in population_sse]

    population = ga.crossover(
        population=population,
        fitness=population_fitness,
        offspring_size=POPULATION_SIZE,
    )

    population_sse = ga.population_sse(population=population)
